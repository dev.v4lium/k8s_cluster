```
vagrant init
```
# remplacer le Vagrantfile par celui-ci
```
vagrant up
```
# sur chaque machine, editer /etc/fstab et commenter la ligne swap :
```
sudo nano /etc/fstab
sudo swapoff -a
```
# sur master :
```
sudo kubeadm init --apiserver-advertise-address=<master ip> --pod-network-cidr=<subnetwork ip>/<mask>
```
# sur nodes :
```
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
```
# ajouter un plugin réseau (calico, flanel, ...)
# sur le master :
```
sudo kubeadm token create --print-join-command
```
# sur les nodes:
```
sudo kubeadm join <master ip>:6443 --token …
```

